EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Transistor_BJT:TIP127 Q1
U 1 1 61E0EBD2
P 5650 3450
F 0 "Q1" H 5857 3404 50  0000 L CNN
F 1 "TIP147" H 5857 3495 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 5850 3375 50  0001 L CIN
F 3 "https://www.onsemi.com/pub/Collateral/TIP120-D.PDF" H 5650 3450 50  0001 L CNN
	1    5650 3450
	1    0    0    1   
$EndComp
$Comp
L Device:R_US R3
U 1 1 61E10350
P 5750 3850
F 0 "R3" H 5818 3896 50  0000 L CNN
F 1 "33" H 5818 3805 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_Power_L50.0mm_W9.0mm_P55.88mm" V 5790 3840 50  0001 C CNN
F 3 "~" H 5750 3850 50  0001 C CNN
	1    5750 3850
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R2
U 1 1 61E10B30
P 5000 3200
F 0 "R2" H 5068 3246 50  0000 L CNN
F 1 "1k" H 5068 3155 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0309_L9.0mm_D3.2mm_P2.54mm_Vertical" V 5040 3190 50  0001 C CNN
F 3 "~" H 5000 3200 50  0001 C CNN
	1    5000 3200
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R1
U 1 1 61E115A4
P 5450 2600
F 0 "R1" V 5245 2600 50  0000 C CNN
F 1 "1k" V 5336 2600 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0309_L9.0mm_D3.2mm_P2.54mm_Vertical" V 5490 2590 50  0001 C CNN
F 3 "~" H 5450 2600 50  0001 C CNN
	1    5450 2600
	0    1    1    0   
$EndComp
$Comp
L Device:D D1
U 1 1 61E159FE
P 5450 2950
F 0 "D1" H 5450 2733 50  0000 C CNN
F 1 "D" H 5450 2824 50  0000 C CNN
F 2 "Diode_THT:D_A-405_P10.16mm_Horizontal" H 5450 2950 50  0001 C CNN
F 3 "~" H 5450 2950 50  0001 C CNN
	1    5450 2950
	-1   0    0    1   
$EndComp
$Comp
L Device:CP1 C1
U 1 1 61E16E1F
P 6300 3250
F 0 "C1" H 6415 3296 50  0000 L CNN
F 1 "1000" H 6415 3205 50  0000 L CNN
F 2 "Capacitor_THT:CP_Axial_L38.0mm_D21.0mm_P44.00mm_Horizontal" H 6300 3250 50  0001 C CNN
F 3 "~" H 6300 3250 50  0001 C CNN
	1    6300 3250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0101
U 1 1 61E178BA
P 6300 3600
F 0 "#PWR0101" H 6300 3350 50  0001 C CNN
F 1 "GND" H 6305 3427 50  0000 C CNN
F 2 "" H 6300 3600 50  0001 C CNN
F 3 "" H 6300 3600 50  0001 C CNN
	1    6300 3600
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0102
U 1 1 61E17ED0
P 5750 4100
F 0 "#PWR0102" H 5750 3850 50  0001 C CNN
F 1 "GND" H 5755 3927 50  0000 C CNN
F 2 "" H 5750 4100 50  0001 C CNN
F 3 "" H 5750 4100 50  0001 C CNN
	1    5750 4100
	1    0    0    -1  
$EndComp
Wire Wire Line
	5000 3050 5000 2950
Wire Wire Line
	5000 2600 5300 2600
Wire Wire Line
	5300 2950 5000 2950
Connection ~ 5000 2950
Wire Wire Line
	5000 2950 5000 2600
Wire Wire Line
	5000 3350 5000 3450
Wire Wire Line
	5000 3450 5450 3450
Wire Wire Line
	5750 3250 5750 2950
Wire Wire Line
	5750 2600 5600 2600
Wire Wire Line
	5600 2950 5750 2950
Connection ~ 5750 2950
Wire Wire Line
	5750 2950 5750 2600
Wire Wire Line
	5750 2600 6300 2600
Connection ~ 5750 2600
Wire Wire Line
	6300 2600 6300 3100
Wire Wire Line
	6300 3400 6300 3600
Wire Wire Line
	5750 3650 5750 3700
Wire Wire Line
	5750 4000 5750 4100
$Comp
L Connector_Generic:Conn_02x02_Odd_Even J1
U 1 1 61E1ED57
P 5550 1700
F 0 "J1" H 5600 1917 50  0000 C CNN
F 1 "Conn_02x02_Odd_Even" H 5600 1826 50  0000 C CNN
F 2 "Connector_Molex:Molex_Mega-Fit_76825-0004_2x02_P5.70mm_Horizontal" H 5550 1700 50  0001 C CNN
F 3 "~" H 5550 1700 50  0001 C CNN
	1    5550 1700
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0103
U 1 1 61E1FCB4
P 6000 2050
F 0 "#PWR0103" H 6000 1800 50  0001 C CNN
F 1 "GND" H 6005 1877 50  0000 C CNN
F 2 "" H 6000 2050 50  0001 C CNN
F 3 "" H 6000 2050 50  0001 C CNN
	1    6000 2050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0104
U 1 1 61E20284
P 5300 2050
F 0 "#PWR0104" H 5300 1800 50  0001 C CNN
F 1 "GND" H 5305 1877 50  0000 C CNN
F 2 "" H 5300 2050 50  0001 C CNN
F 3 "" H 5300 2050 50  0001 C CNN
	1    5300 2050
	1    0    0    -1  
$EndComp
Wire Wire Line
	5850 1800 6300 1800
Wire Wire Line
	5000 1800 5000 2600
Connection ~ 5000 2600
Wire Wire Line
	6300 1800 6300 2600
Connection ~ 6300 2600
Text GLabel 4800 1800 0    50   Input ~ 0
Power_in
Text GLabel 6500 1800 2    50   Input ~ 0
To_motor
Wire Wire Line
	4800 1800 5000 1800
Wire Wire Line
	6300 1800 6500 1800
Connection ~ 6300 1800
Wire Wire Line
	5300 2050 5300 1700
Wire Wire Line
	5300 1700 5350 1700
Wire Wire Line
	6000 2050 6000 1700
Wire Wire Line
	6000 1700 5850 1700
Wire Wire Line
	5000 1800 5350 1800
Connection ~ 5000 1800
$EndSCHEMATC
