Returned Energy Dump
====================

In 2017, [Gecko Drive published a
circuit to dump returned energy from a
servo](https://www.geckodrive.com/support/returned-energy-dump),
or, what Mark Rehorst called [Bank Account
Protection Circuit for Servo/Stepper
Motors](https://drmrehorst.blogspot.com/2022/05/bank-account-protection-circuit-for.html)
when he published a board design. This design started from his work.

This is one possible board implementing that circuit, using parts
currently available as of the time of design.  It uses a TO-220
dual Schottky package as that seems to be widely available from a
number of sources.

## Parts

Prices current at Mouser at time of initial writing, not including
shipping. PRs accepted for other sources, including equivalent parts
as well as exact matches.

| Part    | Value/description    | manufacturer | part number         | Mouser            | Digikey            | qty | $ each | ext      |
| ----    | -----------------    | ------------ | -----------         | ------            | -------            | --- | ------ | ---      |
| R1, R2  | 1k, 1W               | Bournes      | WS1A1001J           | 652-WS1A1001J     | ~ ALSR011K000JE12  | 2   | 0.53   |   1.06   |
| R3      | 33 Ohms, 10W         | Yageo        | SQP10AJB-33R        | 603-SQP10AJB-33R  | 33W-10-ND          | 1   | 0.80   |   0.80   |
| R4¹     | 2k4, 1W              | Vishay       | AC01000002401JA100  | 594-AC01W2K400J   |                    | 1   | 0.77   |   0.77   |
| R5¹     | 220R, 0.25W          | TE Conn.     | LR0204F220R         | 279-LR0204F220R   |                    | 1   | 0.13   |   0.13   |
| R6¹²    | 4k7, 0.25W           | Vishay       | CCF074K70GKE36      | 71-CCF074K70GKE36 |                    | 1   | 0.18   |   0.18   |
| D1      | 100V 15A             | Vishay       | VS-30CTQ100-M3      | 78-VS-30CTQ100-M3 | VS-30CTQ100-M3-ND  | 1   | 1.77   |   1.77   |
| D2¹     | 39V ~1W Zener        | Vishay       | 1N4754A-TAP         | 78-1N4754A-TAP    |                    | 1   | 0.34   |   0.34   |
| D3, D4¹ | 0.7 Vf               | ON Semi      | 1N914               | 512-1N914         |                    | 2   | 0.10   |   0.10   |
| Q1      | 100V PNP Darlington  | ON Semi      | BDW94C              | 512-BDW94C        | BDW94CFS-ND        | 1   | 1.06   |   1.06   |
| C1      | 1000uF 100V          | Nichicon     | UVR2A102MRD6        | 647-UVR2A102MRD6  | 493-14310-ND       | 1   | 3.43   |   3.43   |
| U1¹     | 6N139 Optocoupler    | Vishay       | 6N139               | 782-6N139         |                    | 1   | 1.56   |   1.56   |
|         |                      |              |                     |                   |                    |     |        |**11.20** |

¹These parts required only for an optional fault sense circuit.
If you want to raise an error, provide a reference voltage up
to 18VDC on `Vcc`, and sense connection between `Alarm` and `GNDREF`.

²Resistor R6 is optional; it should not normally be required if
you have a pull-up resistor enabled externally, for example if
you are monitoring this with a microcontroller with an internal
pull-up resistor.

## Modifications

The fault circuit is designed to trigger around 44.5V from the motor.
You can increase or decrease this by changing the value of the Zener
D2 from 39V. Note that this will also change the voltage across R4,
and thus current through the optocoupler; to achieve your desired
sensitivity, you may also have to adjust the R4 value.  The 6N139
Optocoupler will turn on at 1.6mA or less, and can tolerate up
to 25mA.

The D3/D4 pair is meant to limit the voltage across the optocoupler
to 1.4V. The optocoupler has a maximum forward voltage of 1.7V.

## Installation

The board is 65mm x 46mm, with M3 mounting holes at four corners
of a 57mm x 53mm grid. The holes are at X stations 18mm and 49mm,
and Y stations 4mm and 61mm.

Solder motor power wires directly to the board. From the
power supply to the RED, twist together one power (preferably with
an inline fuse of sufficient capacity) and two ground
wires. Terminate power and ground on the RED. Add a motor supply
wire from the RED, and continue to twist with the remaining ground
to the powered device controller.

Install an appropriate MOV across the inductive device (motor)
terminals as close as practical to the device itself.

This is implemented in [KiCad](https://www.kicad.org/) 6.0.
